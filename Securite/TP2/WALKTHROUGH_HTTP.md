#### **Q.** Lorsque le site est actif, naviguer à l'adresse http://localhost. La page indique une adresse IP. Laquelle ?
`A. 127.0.0.1`

#### **Q.** Quelle option de tcpdump permet d’indiquer que l’interface capturée est localhost ?
`A. sudo tcpdump -i [nom-interface]`

#### **Q.** L’option -s permet de préciser la taille maximale des paquets capturer. La valeur par défaut est 264144 octets. Mais c’est beaucoup trop lorsqu’il s’agit de paquets IP. Quelle valeur doit-on donner pour capturer des paquets IP sans risquer de les tronquer ?


#### **Q.** Trouvez l’option qui vous permettra de placer les paquets dans un fichier que vous appellerez nginx_http.pcap
`A. sudo tcpdump -w [nom-fichier]`


> Capturer des paquets sur l'interface lo (localhost) et sauvgarder dans le fichier 'nginx_http.pcap'

`A. sudo tcpdump -i lo -w nginx_http.pcap`

#### **Q.** Quels numéros ont les paquets contenant la requête http et sa réponse ?
`A. 112 jusqu'à 146`

#### **Q.** De quel type est la requête http ?
`A. HTTP`

#### **Q.** Pouvez-vous lire le contenu de la réponse ?
`A. Oui`

#### **Q.** Quel est le résultat de cette commande ?
 `curl -d "username=Machin&password=123456" -X POST http://localhost/Bonjour.php`

 `A. Vérification de ton mot de passe : 123456`

#### **Q.** Pouvez-vous lire les valeurs saisies dans les champs ?
`A. Oui`